//
//  CustomTabBarController.swift
//  Benchmark
//
//  Created by Andrei Trukhan on 4/21/20.
//  Copyright © 2020 Andrei Trukhan. All rights reserved.
//

import UIKit

class CustomTabBarController: UITabBarController {
    
    private let mainViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "main") as! MainViewController
    private var historyViewController = HistoryViewController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTabBar()
    }
    
    private func setupTabBar() {
        mainViewController.tabBarItem.image = UIImage(named: StuffName.home)
        mainViewController.tabBarItem.title = StuffName.home
        historyViewController.tabBarItem.image = UIImage(named: StuffName.history)
        historyViewController.tabBarItem.title = StuffName.history
        
         viewControllers = [mainViewController, historyViewController].map { UINavigationController(rootViewController: $0)
        }
    }
}
