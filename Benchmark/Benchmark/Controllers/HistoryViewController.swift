//
//  historyViewController.swift
//  Benchmark
//
//  Created by Andrei Trukhan on 4/21/20.
//  Copyright © 2020 Andrei Trukhan. All rights reserved.
//

import UIKit
import CoreData

class HistoryViewController: UIViewController {
    
    private let tableView = UITableView()
    private var frc: NSFetchedResultsController<TestResult>?
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        setupTableView()
        
        navigationItem.title = StuffName.history
        navigationController?.navigationBar.prefersLargeTitles = true
        navigationItem.largeTitleDisplayMode = .always
        navigationController?.navigationBar.barTintColor = .customGray
        navigationController?.navigationBar.backgroundColor = .customGray
        navigationController?.navigationBar.largeTitleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white, NSAttributedString.Key.kern : 1.07, NSAttributedString.Key.font: UIFont(name: "Helvetica Bold", size: 41)!]
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white, NSAttributedString.Key.kern : 0.45, NSAttributedString.Key.font: UIFont(name: "Helvetica Bold", size: 18)!]
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .edit, target: self, action: #selector(edit))
        navigationItem.rightBarButtonItem?.setTitleTextAttributes([NSAttributedString.Key.foregroundColor : UIColor.customOrange, NSAttributedString.Key.kern : 0.85, NSAttributedString.Key.font: UIFont(name: "Helvetica Bold", size: 16)!], for: .normal)
        
        view.backgroundColor = .customGray
        
        tableView.delegate = self
        tableView.dataSource = self
        
        let request: NSFetchRequest<TestResult> = TestResult.fetchRequest()
        let dateSortDescriptor = NSSortDescriptor(keyPath: \TestResult.testDate, ascending: false)
        request.sortDescriptors = [dateSortDescriptor]
        do {
            frc = NSFetchedResultsController(fetchRequest: request, managedObjectContext: CoreDataService.shared.context, sectionNameKeyPath: nil, cacheName: nil)
            
            try frc?.performFetch()
        } catch {
            fatalError("Coudn't load FetchedResultController \(error)")
        }

        tableView.register(UINib(nibName: "TitleCell", bundle: nil), forCellReuseIdentifier: "cell")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        frc?.delegate = self
        tableView.reloadData()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        frc?.delegate = nil
    }
    
    @objc private func edit() {
        tableView.isEditing.toggle()
    }
    
    func setupTableView() {
        view.addSubview(tableView)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.topAnchor.constraint(equalTo: view.topAnchor, constant: 50.0).isActive = true
        tableView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        tableView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
    }
    
}

// MARK: - UITableViewDataSource

extension HistoryViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        frc?.fetchedObjects?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as? TitleCell else { fatalError() }
        guard let testResult = frc?.object(at: indexPath) else { fatalError("Inconsistency") }
        guard let date = testResult.testDate, let time = testResult.testTime else {return cell}
        cell.arraySizeLabel?.text = "Array size: \(testResult.arraySize)"
        cell.partsSizeLabel?.text = "Number of parts: \(testResult.partsSize)"
        let formatter = DateFormatter()
        formatter.timeZone = TimeZone.autoupdatingCurrent
        formatter.dateFormat = "HH:mm, MMMM dd, yyyy"
        cell.dateLabel?.text = formatter.string(from: date)
        formatter.dateFormat = "m:ss:SSS"
        cell.timeLabel?.text = formatter.string(from: time)
        return cell
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        
        guard let testResult = frc?.object(at: indexPath) else { return }
        CoreDataService.shared.context.delete(testResult)
        CoreDataService.shared.saveContext()
    }
}

// MARK: - UITableViewDelegate

extension HistoryViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
// MARK: - NSFetchedResultsControllerDelegate

extension HistoryViewController: NSFetchedResultsControllerDelegate {

    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.beginUpdates()
    }

    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.endUpdates()
    }

    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        switch type {
        case .delete:
            guard let indexPath = indexPath else { break }
            tableView.deleteRows(at: [indexPath], with: .left)
        default:
            break
        }
    }
}
