//
//  ViewController.swift
//  Benchmark
//
//  Created by Andrei Trukhan on 4/2/20.
//  Copyright © 2020 Andrei Trukhan. All rights reserved.
//

import UIKit

class MainViewController: UIViewController {
    
    @IBOutlet var backgroudView: UIView!
    @IBOutlet weak var partsLabel: UILabel!
    @IBOutlet weak var arraySizeLabel: UILabel!
    @IBOutlet weak var startButton: UIButton!
    @IBOutlet weak var arraySizeSlider: UISlider!
    @IBOutlet weak var partsSlider: UISlider!
    @IBOutlet weak var labelArray: UILabel!
    @IBOutlet weak var labelParts: UILabel!
    
    private let titleName = StuffName.benchmark
    private let startButtonName = StuffName.start
    private var partsValue = 1
    private var arraySize = 1000
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    @IBAction func changeArraySize(_ sender: UISlider) {
        arraySizeSlider.value =  round(arraySizeSlider.value)
        let pow = arraySizeSlider.value
        arraySize = Int(1000 * powf(10, pow))
        arraySizeLabel.text = String(arraySize)
    }
    
    @IBAction func changeParts(_ sender: UISlider) {
        partsSlider.value = round(partsSlider.value)
        let pow = partsSlider.value
        partsValue = Int(powf(2.0, pow))
        partsLabel.text = String(partsValue)
    }
    
    @IBAction func startButton(_ sender: UIButton) {
        let pVC = PresentedViewController()
        pVC.configureView(arraySize: arraySize, parts: partsValue)
        let nPVC = UINavigationController(rootViewController: pVC)
        present(nPVC, animated: true)
    }

    private func setupView() {
        partsLabel.configure(text: String(partsValue), font: UIFont(name: "Helvetica", size: 43), kern: 0.47, textColor: .customOrange)
        arraySizeLabel.configure(text: String(arraySize), font: UIFont(name: "Helvetica", size: 43), kern: 0.47, textColor: .customOrange)
        labelParts.configure(font: UIFont(name: "Helvetica", size: 17), kern: 0.19, textColor: .customLightGray)
        labelArray.configure(font: UIFont(name: "Helvetica", size: 17), kern: 0.19, textColor: .customLightGray)
        startButton.configure(text: startButtonName, font: UIFont(name: "Helvetica Bold", size: 25), kern: 3.07, textColor: .white, backgroundColor: .customGray, cornerRadius: 25)
        arraySizeSlider.minimumTrackTintColor = .paleTeal
        partsSlider.minimumTrackTintColor = .paleTeal
        
        backgroudView.backgroundColor = .customGray
        
        navigationItem.title = titleName
        navigationController?.navigationBar.prefersLargeTitles = true
        navigationItem.largeTitleDisplayMode = .always
        navigationController?.navigationBar.largeTitleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white, NSAttributedString.Key.kern : 1.07, NSAttributedString.Key.font: UIFont(name: "Helvetica Bold", size: 41)!]
    }
}


