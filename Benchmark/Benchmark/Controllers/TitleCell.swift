//
//  TitleCell.swift
//  Benchmark
//
//  Created by Andrei Trukhan on 4/21/20.
//  Copyright © 2020 Andrei Trukhan. All rights reserved.
//

import UIKit

class TitleCell: UITableViewCell {

    @IBOutlet weak var partsSizeLabel: UILabel!
    @IBOutlet weak var arraySizeLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        partsSizeLabel.configure(font: UIFont(name: "Helvetica Bold", size: 12), kern: 0.64, textColor: .customGray)
        arraySizeLabel.configure(font: UIFont(name: "Helvetica Bold", size: 12), kern: 0.64, textColor: .customGray)
        dateLabel.configure(font: UIFont(name: "Helvetica Bold", size: 10), kern: 0.53, textColor: .customGray)
        timeLabel.configure(font: UIFont(name: "Helvetica Bold", size: 30), kern: 1.02, textColor: .paleTeal)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()

        partsSizeLabel.configure(text: "")
        arraySizeLabel.configure(text: "")
        dateLabel.configure(text: "")
        timeLabel.configure(text: "")
    }
}
