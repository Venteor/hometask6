//
//  PresentedViewController.swift
//  Benchmark
//
//  Created by Andrei Trukhan on 4/3/20.
//  Copyright © 2020 Andrei Trukhan. All rights reserved.
//

import UIKit

class PresentedViewController: UIViewController {
    
    enum State {
        case executing
        case finished
    }
    
    private let benchmarkService = BenchmarkService.shared
    private var form = DateFormatter()
    private var timer = Timer()
    private var startTime: Double = 0
    private var currentTime = Date()
    private var randomArray = [Double]()
    private var date = Date()
    private var historyService = HistoryService(context: CoreDataService.shared.context)
    private var state: State = .executing {
        didSet{
            updateViewState()
        }
    }
    
    private var titleName = StuffName.measuring
    private var closeButtonName = StuffName.stop
    private var arraySize: Int = 1000
    private var parts: Int = 1
    
    
    
    @IBOutlet weak var currentTimeLabel: UILabel!
    @IBOutlet weak var labelArrayPr: UILabel!
    @IBOutlet weak var arraySizePr: UILabel!
    @IBOutlet weak var labelPartsPr: UILabel!
    @IBOutlet weak var partsPr: UILabel!
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        state = .executing
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        DispatchQueue.global().async {
            
            self.randomArray = self.benchmarkService.makeArray(size: self.arraySize)
                guard self.state == .executing else { return }
                self.startTesting()
        }
    }
    
    private func startTesting() {
        DispatchQueue.main.async {
            self.date = Date()
            self.startTime = self.date.timeIntervalSinceReferenceDate
            
            
            self.timer = Timer.scheduledTimer(timeInterval: 0.0, target: self, selector:#selector(self.tick) , userInfo: nil, repeats: true)
            self.closeButton.isEnabled = true
            
            DispatchQueue.global().async {
                self.benchmarkService.startBenchmark(array: self.randomArray, parts: self.parts) { [weak self]  in
                    guard let self = self else { return }
                    self.timer.invalidate()
                    self.state = .finished
                    self.historyService.createHistory(arraySize: self.arraySize, partsSize: self.parts, testTime: self.currentTime, testDate: self.date)
                }
            }
        }
    }
    
    func configureView(arraySize: Int, parts: Int) {
        self.arraySize = arraySize
        self.parts = parts
    }
    
    @IBAction func close(_ sender: UIButton) {
        benchmarkService.stopTest()
        closeButton.isEnabled = false
        self.presentingViewController?.dismiss(animated: true, completion: nil)
    }
    
    @objc private func tick() {
        let formatter = DateFormatter()
        formatter.dateFormat = "m:ss:SSS"
        
        let time = Date().timeIntervalSinceReferenceDate - startTime
        currentTime = Date(timeIntervalSinceReferenceDate: time)
        
        currentTimeLabel.text = formatter.string(from: currentTime)
    }
    
    private func updateViewState() {
        switch state {
        case .executing:
            
            view.backgroundColor = .customOrange
            
            activityIndicator.startAnimating()
            activityIndicator.hidesWhenStopped = true
            
            navigationItem.title = titleName
            navigationController?.navigationBar.prefersLargeTitles = true
            navigationItem.largeTitleDisplayMode = .always
            navigationController?.navigationBar.largeTitleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.customGray, NSAttributedString.Key.kern : 1.07, NSAttributedString.Key.font: UIFont(name: "Helvetica Bold", size: 41)!]
            
            currentTimeLabel.configure(text: "0:00:000", font: UIFont(name: "Helvetica", size: 43), kern: 0.47, textColor: .customOrange)
            currentTimeLabel.layer.borderColor = UIColor.customGray.cgColor
            currentTimeLabel.layer.borderWidth = 3
            currentTimeLabel.layer.cornerRadius = 5
            
            labelArrayPr.configure(text: nil, font: UIFont(name: "Helvetica", size: 17), kern: 0.19, textColor: .customLightGray)
            labelPartsPr.configure(text: nil, font: UIFont(name: "Helvetica", size: 17), kern: 0.19, textColor: .customLightGray)
            arraySizePr.configure(text: String(arraySize), font: UIFont(name: "Helvetica", size: 43), kern: 0.47, textColor: .customGray)
            partsPr.configure(text: String(parts), font: UIFont(name: "Helvetica", size: 43), kern: 0.47, textColor: .customGray)
            
            closeButton.configure(text: closeButtonName, font: UIFont(name: "Helvetica Bold", size: 25), kern: 3.07, textColor: .white, backgroundColor: .customGray, cornerRadius: 25)
            closeButton.isEnabled = false
        case .finished:
            DispatchQueue.main.async {
                self.currentTimeLabel.layer.borderColor = UIColor.paleTeal.cgColor
                self.closeButtonName = StuffName.close
                self.closeButton.changeTitle(text: self.closeButtonName)
                self.titleName = StuffName.result
                self.navigationItem.title = self.titleName
                self.view.backgroundColor = .paleTeal
                self.currentTimeLabel.textColor = .customGray
                self.activityIndicator.stopAnimating()
            }
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        benchmarkService.stopTest()
        super.viewDidDisappear(true)
        timer.invalidate()
        state = .finished
        closeButton.isEnabled = false
    }
}
