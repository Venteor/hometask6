//
//  StuffName.swift
//  Benchmark
//
//  Created by Andrei Trukhan on 4/3/20.
//  Copyright © 2020 Andrei Trukhan. All rights reserved.
//

import Foundation

struct StuffName {
    
    static let benchmark = "Benchmark"
    static let measuring = "Measuring"
    static let result = "Result"
    static let home = "Home"
    static let history = "History"
    
    static let start = "START"
    static let stop = "STOP"
    static let close = "CLOSE"
}
