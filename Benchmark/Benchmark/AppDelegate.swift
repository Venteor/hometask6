//
//  AppDelegate.swift
//  Benchmark
//
//  Created by Andrei Trukhan on 4/2/20.
//  Copyright © 2020 Andrei Trukhan. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {

        UITabBar.appearance().tintColor = .customOrange
        
        return true
    }
}

