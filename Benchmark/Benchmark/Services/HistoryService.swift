//
//  HistoryService.swift
//  Benchmark
//
//  Created by Andrei Trukhan on 4/23/20.
//  Copyright © 2020 Andrei Trukhan. All rights reserved.
//

import Foundation
import CoreData

final class HistoryService {
    
    private let context: NSManagedObjectContext
    
    init(context: NSManagedObjectContext) {
        self.context = context
    }
    
    @discardableResult
    func createHistory(arraySize: Int, partsSize: Int, testTime: Date, testDate: Date) -> TestResult {
        let historyRow = TestResult(context: context)
        historyRow.arraySize = Int64(arraySize)
        historyRow.testTime = testTime
        historyRow.testDate = testDate
        historyRow.partsSize = Int64(partsSize)
        CoreDataService.shared.saveContext()
        return historyRow
    }    
}

