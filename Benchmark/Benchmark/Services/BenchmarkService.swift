//
//  BenchmarkService.swift
//  Benchmark
//
//  Created by Andrei Trukhan on 4/4/20.
//  Copyright © 2020 Andrei Trukhan. All rights reserved.
//

import Foundation

final class  BenchmarkService {
    
    enum State {
        case executing
        case finished
    }
    
    static let shared = BenchmarkService()
    
    private let semafore = DispatchSemaphore(value: 1)
    private var state: State = .executing
    
    func startBenchmark(array: [Double], parts: Int, completion: @escaping (() -> Void)) {
        state = .executing
        let group = DispatchGroup()
        let queue = DispatchQueue.global(qos: .userInitiated)
        let chunkedArray = array.chunked(in: parts)
        var changedArray = [[Double]]()
        
        for chunk in chunkedArray {
            group.enter()
            queue.async {
                var changedChunk = chunk
                for index in  0...changedChunk.count - 1 {
                    guard self.state == .executing else { break }
                    let i = Double(index)
                    changedChunk[index] = changedChunk[index] * sin(0.2 + i / 1.0) * cos(0.2 + i / 2.0) * cos(0.4 + i / 3.0)
                }
                self.semafore.wait()
                changedArray.append(changedChunk)
                group.leave()
                self.semafore.signal()
            }
        }
        group.notify(queue: queue) {
            let resultArray = changedArray.flatMap { $0 }
            completion()
        }
    }
    
    func stopTest() {
        state = .finished
    }
    
    func makeArray(size: Int) -> [Double] {
        (0..<size).map { _ in .random(in: 1.0...100.0) }
    }
}
