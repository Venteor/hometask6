//
//  CoreDataService.swift
//  Benchmark
//
//  Created by Andrei Trukhan on 4/22/20.
//  Copyright © 2020 Andrei Trukhan. All rights reserved.
//

import Foundation
import CoreData

final class CoreDataService {

    static var shared = CoreDataService()

    var context: NSManagedObjectContext {
        persistentContainer.viewContext
    }

    private lazy var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "BenchmarkModel")
        container.loadPersistentStores { storeDescription, error in
            if let error = error as NSError? { fatalError("Unresolved error \(error), \(error.userInfo)") }
        }
        return container
    }()

    // MARK: - Core Data Saving support

    func saveContext() {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
}
