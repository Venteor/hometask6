//
//  TestResult+CoreDataProperties.swift
//  Benchmark
//
//  Created by Andrei Trukhan on 4/23/20.
//  Copyright © 2020 Andrei Trukhan. All rights reserved.
//
//

import Foundation
import CoreData


extension TestResult {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<TestResult> {
        return NSFetchRequest<TestResult>(entityName: "TestResult")
    }

    @NSManaged public var arraySize: Int64
    @NSManaged public var partsSize: Int64
    @NSManaged public var testDate: Date?
    @NSManaged public var testTime: Date?

}
