//
//  Extensions.swift
//  Benchmark
//
//  Created by Andrei Trukhan on 4/7/20.
//  Copyright © 2020 Andrei Trukhan. All rights reserved.
//

import UIKit

extension UIColor {
    
    class var customGray: UIColor { self.init(red: 0.34, green: 0.38, blue: 0.47, alpha: 1.0)}
    class var customLightGray: UIColor { self.init(red: 0.49, green: 0.50, blue: 0.51, alpha: 1.0)}
    class var customOrange: UIColor { self.init(red: 0.98, green: 0.50, blue: 0.12, alpha: 1.0)}
    class var paleTeal: UIColor { self.init(red: 0.45, green: 0.78, blue: 0.63, alpha: 1.0)}
}

extension UIButton {
    
    func configure(text: String? = nil, font: UIFont? = nil, kern: CGFloat? = nil, textColor: UIColor? = nil, backgroundColor: UIColor? = nil, cornerRadius: CGFloat? = nil) {
        
        if let text = text {
            self.setTitle(text, for: .normal)
        }
        if let font = font {
            self.titleLabel?.font = font
        }
        if let kern = kern, let text = titleLabel?.text {
            let attributedString = NSMutableAttributedString(string: text)
            attributedString.addAttribute(NSAttributedString.Key.kern, value: kern, range: NSRange(location: 0, length: attributedString.length - 1))
            self.setAttributedTitle(attributedString, for: .normal)
        }
        if let textColor = textColor {
            self.titleLabel?.textColor = textColor
        }
        if let backgroundColor = backgroundColor {
            self.backgroundColor = backgroundColor
        }
        if let cornerRadius = cornerRadius {
            self.layer.cornerRadius = cornerRadius
        }
    }
    
    func changeTitle(text: String) {
        if let attributedTitle = self.attributedTitle(for: .normal) {
            let mutableAttributedTitle = NSMutableAttributedString(attributedString: attributedTitle)
            mutableAttributedTitle.replaceCharacters(in: NSMakeRange(0, mutableAttributedTitle.length), with: text)
            self.setAttributedTitle(mutableAttributedTitle, for: .normal)
        }
    }
}

extension UILabel {
    
    func configure(text: String? = nil, font: UIFont? = nil, kern: CGFloat? = nil, textColor: UIColor? = nil) {
        if let text = text {
            self.text = text
        }
        if let font = font {
            self.font = font
        }
        if let kern = kern, let text = self.text {
            let attributedString = NSMutableAttributedString(string: text)
            attributedString.addAttribute(NSAttributedString.Key.kern, value: kern, range: NSRange(location: 0, length: attributedString.length - 1))
            attributedText = attributedString
        }
        if let textColor = textColor {
            self.textColor = textColor
        }
    }
}

extension Array {
    
    func chunked(in parts: Int) -> [[Element]] {
        if count % parts != 0 {
            let maxSize = Int(ceil(Double(count) / Double(parts)))
            let minSize = count / parts
            let countMinSize = abs(count - maxSize * parts)
            let countMaxSize = parts - countMinSize
            let maxSizeBorder = countMaxSize * maxSize
            let arrayMaxSize = stride(from: 0, to: maxSizeBorder, by: maxSize).map {
                Array(self[$0 ..< Swift.min($0 + maxSize, maxSizeBorder)])
            }
            let arrayMinSize = stride(from: maxSizeBorder, to: count, by: minSize).map {
                Array(self[$0 ..< Swift.min($0 + minSize, count)])
            }
            return arrayMaxSize + arrayMinSize
        }
        let size = count/parts
        return stride(from: 0, to: count, by: size).map {
            Array(self[$0 ..< Swift.min($0 + size, count)])
        }
    }
}
